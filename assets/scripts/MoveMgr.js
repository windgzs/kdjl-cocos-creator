var MoveMgr = {
    lPreCard: [],
    lPrePos: [],
    lPreRow: [],
    _delegate: null,

    init(delt){
        this._delegate = delt;
    },

    removeAll(){
        this.lPreCard = [];
        this.lPrePos = [];
        this.lPreRow = [];
    },

    onMoveCard(){
        var tIdx = [this.lPreCard.length - 1, this.lPrePos.length - 1, this.lPreRow.length - 1];
        var preCard = this.lPreCard [tIdx [0]];
        var prePos = this.lPrePos [tIdx [1]];
        var preRow = this.lPreRow [tIdx [2]];
        this._delegate.moveCards (preCard, prePos, preRow);
        this.lPreCard.splice (tIdx [0], 1);
        this.lPrePos.splice (tIdx [1], 1);
        this.lPreRow.splice (tIdx [2], 1);
    },

    addCard(card){
        this.lPreCard.push(card);
        this.lPrePos.push(card.getPos ());
        this.lPreRow.push(card.getRow ());
    },

    getPreCard(){
        return this.lPreCard [this.lPreCard.length - 1];
    },

    getCountCards(){
        return this.lPreCard.length;
    },
};

module.exports = MoveMgr;